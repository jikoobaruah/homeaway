package com.example.foursquare.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

public class Categories implements Parcelable {

    private String pluralName;

    private String name;

    private Icon icon;

    private String id;

    private String shortName;

    private String primary;

    public Categories(){}

    protected Categories(Parcel in) {
        pluralName = in.readString();
        name = in.readString();
        icon = in.readParcelable(Icon.class.getClassLoader());
        id = in.readString();
        shortName = in.readString();
        primary = in.readString();
    }

    public static final Creator<Categories> CREATOR = new Creator<Categories>() {
        @Override
        public Categories createFromParcel(Parcel in) {
            return new Categories(in);
        }

        @Override
        public Categories[] newArray(int size) {
            return new Categories[size];
        }
    };

    public String getPluralName ()
    {
        return pluralName;
    }

    public void setPluralName (String pluralName)
    {
        this.pluralName = pluralName;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Icon getIcon ()
    {
        return icon;
    }

    public void setIcon (Icon icon)
    {
        this.icon = icon;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getShortName ()
    {
        return shortName;
    }

    public void setShortName (String shortName)
    {
        this.shortName = shortName;
    }

    public String getPrimary ()
    {
        return primary;
    }

    public void setPrimary (String primary)
    {
        this.primary = primary;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pluralName);
        dest.writeString(name);
        dest.writeParcelable(icon, flags);
        dest.writeString(id);
        dest.writeString(shortName);
        dest.writeString(primary);
    }

    public String getImageUrl() {
        return  icon.getPrefix() + "bg_88" + icon.getSuffix();
    }
}
