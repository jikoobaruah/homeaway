package com.example.foursquare.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;

import com.example.foursquare.R;
import com.example.foursquare.databinding.ActivityDetailsBinding;
import com.example.foursquare.model.dto.Venues;
import com.example.foursquare.view.common.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends BaseActivity {

    private List<Venues> venuesList;
    private int currentPosition;

    private ActivityDetailsBinding binding;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ArrayList<String> likesSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        venuesList = getIntent().getParcelableArrayListExtra(ARGS.LIST);
        currentPosition = getIntent().getIntExtra(ARGS.POSITION, 0);
        likesSet = getIntent().getStringArrayListExtra(ARGS.LIKES);
        if (venuesList == null || venuesList.size() == 0)
            finish();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        binding.container.setAdapter(sectionsPagerAdapter);
        binding.container.setCurrentItem(currentPosition);
    }


    public interface ARGS {
        String LIST = "list";
        String POSITION = "position";
        String LIKES = "likes";
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return DetailsFragment.newInstance(DetailsActivity.this,venuesList.get(position),likesSet.contains(venuesList.get(position).getId()));
        }


        @Override
        public int getCount() {
            return venuesList.size();
        }
    }
}
