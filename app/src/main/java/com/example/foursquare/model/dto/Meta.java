package com.example.foursquare.model.dto;

import com.google.gson.Gson;

public class Meta
{
    private String code;

    private String errorType;

    private String errorDetail;

    private String requestId;

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getRequestId ()
    {
        return requestId;
    }

    public void setRequestId (String requestId)
    {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
