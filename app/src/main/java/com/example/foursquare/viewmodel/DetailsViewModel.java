package com.example.foursquare.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.example.foursquare.BuildConfig;
import com.example.foursquare.R;
import com.example.foursquare.model.dto.VenueLikes;
import com.example.foursquare.model.dto.VenueResponse;
import com.example.foursquare.model.dto.Venues;
import com.example.foursquare.model.repo.DirectionsRepo;
import com.example.foursquare.model.repo.LikesRepo;
import com.example.foursquare.model.repo.PlacesRepo;
import com.example.foursquare.utils.StaticMapsUrlBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DetailsViewModel extends BaseViewModel {

    private final LikesRepo likesRepo;
    private final PlacesRepo placesRepo;
    private DirectionsRepo directionsRepo;

    @Inject
    public DetailsViewModel(@NonNull Application application, DirectionsRepo repo, LikesRepo likesRepo, PlacesRepo placesRepo) {
        super(application);
        this.placesRepo = placesRepo;
        this.likesRepo =likesRepo;
        directionsRepo = repo;
    }

    public void fetchPolyLine(String venueId, double lat, double lon){
        directionsRepo.getDirections(venueId, lat, lon);
    }

    public LiveData<String> getImageLiveData(final Venues venues) {
        return Transformations.map(directionsRepo.getLiveData(), new Function<String, String>() {
            @Override
            public String apply(String input) {
                StaticMapsUrlBuilder builder = new StaticMapsUrlBuilder().addMarker("red","Seattle,WA", BuildConfig.SEATLE_LAT,BuildConfig.SEATLE_LON).addMarker("blue",venues.getName(),venues.getLocation().getLat(),venues.getLocation().getLng()).size(1000,1000);
                if (input != null){
                    builder = builder.path(3,Integer.toHexString(getApplication().getResources().getColor(R.color.colorAccent)),input);
                }
                return builder.build();
            }
        });
    }

    public void onLikeChanged(String venueId, boolean isChecked) {
        likesRepo.onLikeChanged( venueId,  isChecked);
    }

    public LiveData<VenueResponse> getVenueDetails(){
        return  placesRepo.getDetailsLiveData();
    }


    public void fetchDetails(String id) {
        placesRepo.fetchDetails(id);
    }
}
