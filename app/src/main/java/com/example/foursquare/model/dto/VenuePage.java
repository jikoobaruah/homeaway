package com.example.foursquare.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

public class VenuePage implements Parcelable {
    private String id;

    public VenuePage(){}

    protected VenuePage(Parcel in) {
        id = in.readString();
    }

    public static final Creator<VenuePage> CREATOR = new Creator<VenuePage>() {
        @Override
        public VenuePage createFromParcel(Parcel in) {
            return new VenuePage(in);
        }

        @Override
        public VenuePage[] newArray(int size) {
            return new VenuePage[size];
        }
    };

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }
}
