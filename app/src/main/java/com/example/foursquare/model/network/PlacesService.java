package com.example.foursquare.model.network;


import android.arch.lifecycle.LiveData;

import com.example.foursquare.App;
import com.example.foursquare.BuildConfig;
import com.example.foursquare.model.dto.SearchResponse;
import com.example.foursquare.model.dto.VenueResponse;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public interface PlacesService {

    @GET("search")
    Call<SearchResponse> search(@Query("query") String currentSearch);

    @GET("{venueId}")
    Call<VenueResponse> details(@Path("venueId") String venueId);



}
