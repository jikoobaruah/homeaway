package com.example.foursquare;

import android.content.Context;

import com.example.foursquare.di.ApplicationComponent;
import com.example.foursquare.di.DaggerApplicationComponent;
import com.facebook.stetho.Stetho;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;

//        if (BuildConfig.DEBUG) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                    .detectAll()
//                    .penaltyLog()
////                    .penaltyDeath()
//                    .build());
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                    .detectAll()
//                    .penaltyLog()
////                    .penaltyDeath()
//                    .build());
//        }

        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);
//
//        AnalyticsManager.init();
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }
}
