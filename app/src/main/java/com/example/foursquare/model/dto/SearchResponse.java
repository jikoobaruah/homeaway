package com.example.foursquare.model.dto;

import com.google.gson.Gson;

public class SearchResponse {

    private Meta meta;

    private Response response;

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    public Response getResponse ()
    {
        return response;
    }

    public void setResponse (Response response)
    {
        this.response = response;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
