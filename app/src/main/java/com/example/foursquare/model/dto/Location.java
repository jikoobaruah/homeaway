package com.example.foursquare.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

public class Location implements Parcelable {

    private String cc;

    private String country;

    private String address;

    private LabeledLatLngs[] labeledLatLngs;

    private double lng;

    private String distance;

    private String[] formattedAddress;

    private String city;

    private String postalCode;

    private String state;

    private String crossStreet;

    private double lat;

    public Location(){}

    protected Location(Parcel in) {
        cc = in.readString();
        country = in.readString();
        address = in.readString();
        lng = in.readDouble();
        distance = in.readString();
        formattedAddress = in.createStringArray();
        city = in.readString();
        postalCode = in.readString();
        state = in.readString();
        crossStreet = in.readString();
        lat = in.readDouble();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    public String getCc ()
    {
        return cc;
    }

    public void setCc (String cc)
    {
        this.cc = cc;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public LabeledLatLngs[] getLabeledLatLngs ()
    {
        return labeledLatLngs;
    }

    public void setLabeledLatLngs (LabeledLatLngs[] labeledLatLngs)
    {
        this.labeledLatLngs = labeledLatLngs;
    }

    public double getLng ()
    {
        return lng;
    }

    public void setLng (double lng)
    {
        this.lng = lng;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String[] getFormattedAddress ()
    {
        return formattedAddress;
    }

    public void setFormattedAddress (String[] formattedAddress)
    {
        this.formattedAddress = formattedAddress;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getPostalCode ()
    {
        return postalCode;
    }

    public void setPostalCode (String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getCrossStreet ()
    {
        return crossStreet;
    }

    public void setCrossStreet (String crossStreet)
    {
        this.crossStreet = crossStreet;
    }

    public double getLat ()
    {
        return lat;
    }

    public void setLat (double lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cc);
        dest.writeString(country);
        dest.writeString(address);
        dest.writeDouble(lng);
        dest.writeString(distance);
        dest.writeStringArray(formattedAddress);
        dest.writeString(city);
        dest.writeString(postalCode);
        dest.writeString(state);
        dest.writeString(crossStreet);
        dest.writeDouble(lat);
    }
}
