package com.example.foursquare.model.dto;

public class VenueResponse {

    private Meta meta;

    private VenueWrapper response;

    public Meta getMeta() {
        return meta;
    }

    public VenueWrapper getResponse() {
        return response;
    }

    public static class VenueWrapper {

        private Venues venue;



        public Venues getVenue() {
            return venue;
        }
    }
}
