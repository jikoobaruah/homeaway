package com.example.foursquare.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.foursquare.BuildConfig;
import com.google.gson.Gson;

import java.util.List;

public class Venues implements Parcelable {

    private VenuePage venuePage;

    private String name;

    private Location location;

    private String id;

    private List<Categories> categories;

    private String canonicalUrl;

    public Venues(){}

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Venues){
            return  id.equals(((Venues)obj).id);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }




    public String getCanonicalUrl() {
        return canonicalUrl;
    }


    protected Venues(Parcel in) {
        venuePage = in.readParcelable(VenuePage.class.getClassLoader());
        name = in.readString();
        location = in.readParcelable(Location.class.getClassLoader());
        id = in.readString();
        canonicalUrl = in.readString();
        categories = in.createTypedArrayList(Categories.CREATOR);
    }

    public static final Creator<Venues> CREATOR = new Creator<Venues>() {
        @Override
        public Venues createFromParcel(Parcel in) {
            return new Venues(in);
        }

        @Override
        public Venues[] newArray(int size) {
            return new Venues[size];
        }
    };

    public VenuePage getVenuePage ()
    {
        return venuePage;
    }

    public void setVenuePage (VenuePage venuePage)
    {
        this.venuePage = venuePage;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Location getLocation ()
    {
        return location;
    }

    public void setLocation (Location location)
    {
        this.location = location;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public List<Categories> getCategories ()
    {
        return categories;
    }

    public void setCategories (List<Categories> categories)
    {
        this.categories = categories;
    }

    public String getCategoryNames(){
        if (categories == null || categories.size() == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (Categories c : categories ){
            sb.append(c.getName()).append(",");
        }
        return sb.subSequence(0,sb.length()-1).toString();
    }

    public String getDistanceFromSeattle(){
        if (location == null ) return "N/A";

        android.location.Location locationA = new android.location.Location("Seattle");

        locationA.setLatitude(BuildConfig.SEATLE_LAT);
        locationA.setLongitude(BuildConfig.SEATLE_LON);

        android.location.Location locationB = new android.location.Location("point B");

        locationB.setLatitude(location.getLat());
        locationB.setLongitude(location.getLng());

        float distance = locationA.distanceTo(locationB);

        if (distance < 1000) return  String.format("%.2f",distance)+"m";

        return String.format("%.2f",distance/1000f) + "km";
    }

    public String getFormattedAddress(){
        StringBuilder stringBuilder = new StringBuilder();
        if (location.getFormattedAddress() != null){
            for (String s : location.getFormattedAddress()){
                stringBuilder.append(s).append("\n");
            }
            return stringBuilder.toString().trim();
        }
        return "N/A";
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(venuePage, flags);
        dest.writeString(name);
        dest.writeParcelable(location, flags);
        dest.writeString(id);
        dest.writeString(canonicalUrl);
        dest.writeTypedList(categories);
    }
}
