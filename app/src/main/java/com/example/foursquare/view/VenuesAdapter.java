package com.example.foursquare.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bumptech.glide.Glide;
import com.example.foursquare.R;
import com.example.foursquare.databinding.ItemVenueBinding;
import com.example.foursquare.model.dto.Categories;
import com.example.foursquare.model.dto.Venues;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.VenuesHolder> {

    private final Callback callback;
    private List<Venues> venuesList;
    private Set<String> likeSet;

    public VenuesAdapter(Callback callback){
        this.callback = callback;
        venuesList = new ArrayList<>();
        likeSet = new HashSet<>();
    }

    @NonNull
    @Override
    public VenuesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemVenueBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_venue,viewGroup,false);
        return new VenuesHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VenuesHolder venuesHolder, int i) {
        venuesHolder.bindView();
    }


    @Override
    public int getItemCount() {
        return venuesList.size();
    }

    public void setLikesSet(Set<String> likes){


        if (likes != null)
            likeSet.removeAll(likes);

        Set<String> toUnlike = new HashSet<>(likeSet);

        likeSet.clear();

        if (likes != null)
            likeSet = likes;

        String id;
        for (int i =0 ; i< venuesList.size() ; i++){
            id = venuesList.get(i).getId();
            if (likeSet.contains(id) || toUnlike.contains(id))
                notifyItemChanged(i);
        }
    }

    public void updateList(List<Venues> newList){
        final VenuesDiffCallback diffCallback = new VenuesDiffCallback(venuesList, newList);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.venuesList.clear();
        this.venuesList.addAll(newList);
        diffResult.dispatchUpdatesTo(this);
    }

    public void clear() {
        int size = venuesList.size();
        venuesList.clear();
        notifyItemRangeRemoved(0,size);
    }

    public List<Venues> getList() {
        return venuesList;
    }

    public Set<String> getLikesSet() {
        return likeSet;
    }

    public class VenuesHolder extends RecyclerView.ViewHolder{

        private ItemVenueBinding binding;

        public VenuesHolder(@NonNull ItemVenueBinding itemVenueBinding) {
            super(itemVenueBinding.getRoot());
            binding = itemVenueBinding;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(getAdapterPosition(),venuesList);
                }
            });
            binding.cbLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    callback.onCheckChanged(venuesList.get(getAdapterPosition()).getId(),isChecked);
                    if (isChecked)
                        likeSet.add(venuesList.get(getAdapterPosition()).getId());
                    else
                        likeSet.remove(venuesList.get(getAdapterPosition()).getId());

                }
            });
        }

        public void bindView() {
            Venues venues = venuesList.get(getAdapterPosition());
            binding.setVenues(venues);
            binding.setIsLiked(likeSet.contains(venues.getId()));
            binding.executePendingBindings();
            Categories categories = venues.getCategories() == null || venues.getCategories().size() == 0 ? null : venues.getCategories().get(0);
            if (categories != null){
                String imageUrl = categories.getImageUrl();
                Glide.with(binding.ivIcon).load(imageUrl).into(binding.ivIcon);
            }
        }
    }

    interface Callback {
        void onItemClick(int position,List<Venues> venuesList);

        void onCheckChanged(String venues, boolean isChecked);
    }
}
