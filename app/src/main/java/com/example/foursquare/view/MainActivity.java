package com.example.foursquare.view;

import android.animation.ObjectAnimator;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.transition.Explode;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.transition.TransitionListenerAdapter;
import android.support.transition.TransitionManager;
import android.support.transition.TransitionSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;

import com.example.foursquare.BuildConfig;
import com.example.foursquare.R;
import com.example.foursquare.databinding.ActivityMainBinding;
import com.example.foursquare.di.ViewModelFactory;
import com.example.foursquare.model.dto.SearchResult;
import com.example.foursquare.model.dto.Venues;
import com.example.foursquare.utils.Utils;
import com.example.foursquare.view.common.BaseActivity;
import com.example.foursquare.view.common.widget.SearchEditText;
import com.example.foursquare.view.common.widget.VerticalSpaceItemDecoration;
import com.example.foursquare.viewmodel.PlacesViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements VenuesAdapter.Callback, OnMapReadyCallback {

    private static final int TRIGGER_AUTO_COMPLETE = 100;
    private static final long AUTO_COMPLETE_DELAY = 300;
    private static final int THRESHOLD = 2;

    private ActivityMainBinding binding;

    @Inject
    ViewModelFactory viewModelFactory;

    private PlacesViewModel viewModel;
    private String currentSearchString;
    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == TRIGGER_AUTO_COMPLETE) {
                if (!TextUtils.isEmpty(binding.etSearchInput.getText())) {
                    if (!TextUtils.isEmpty(currentSearchString) && currentSearchString.equals(binding.etSearchInput.getText().toString().trim())) return false;
                    currentSearchString = binding.etSearchInput.getText().toString().trim();
                    binding.tvFetchQuery.setText(String.format(getString(R.string.fetching_results_for), currentSearchString));
                    binding.tvFetchQuery.setVisibility(View.VISIBLE);
                    viewModel.fetchPlaces(currentSearchString);
                }
            }
            return false;
        }
    });
    private Observer<SearchResult> searchResultObserver = new Observer<SearchResult>() {
        @Override
        public void onChanged(@Nullable SearchResult searchResult) {
            switch (searchResult.getState()) {
                case IDLE:
                    break;
                case FETCHING:
                    break;
                case FETCH_FAIL:
                    showError(searchResult);
                    break;
                case FETCH_SUCCESS:
                    showSuccess(searchResult);
                    break;
            }
            updateStatusBar(searchResult);
        }
    };
    private Observer<List<String>> likesObserver = new Observer<List<String>>() {
        @Override
        public void onChanged(@Nullable List<String> strings) {
            venuesAdapter.setLikesSet(new HashSet<String>(strings));
        }
    };

    private void showError(SearchResult searchResult) {
        venuesAdapter.clear();
    }

    private void updateStatusBar(SearchResult searchResult) {
        binding.cvStatus.setVisibility(View.VISIBLE);
        if (searchResult.getState() == SearchResult.STATE.FETCH_SUCCESS) {
            binding.tvResultQuery.setText(String.format(getString(R.string.showing_results_for), searchResult.getSearchQuery()));
            binding.tvFetchQuery.setVisibility(currentSearchString.equals(searchResult.getSearchQuery()) ? View.GONE : View.VISIBLE);
        } else if (searchResult.getState() == SearchResult.STATE.FETCH_FAIL) {
            binding.tvResultQuery.setText(searchResult.getError());
            binding.tvFetchQuery.setVisibility(View.GONE);
        }
    }

    private Drawable drawableLeft;
    private Drawable drawableRight;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0) {
                binding.etSearchInput.setCompoundDrawables(drawableLeft, null, null, null);
            } else {
                binding.etSearchInput.setCompoundDrawables(drawableLeft, null, drawableRight, null);
            }

            if (s.length() < THRESHOLD) {
                return;
            }
            handler.removeMessages(TRIGGER_AUTO_COMPLETE);
            handler.sendEmptyMessageDelayed(TRIGGER_AUTO_COMPLETE, AUTO_COMPLETE_DELAY);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private SearchEditText.DrawableClickListener searchDrawableClickListener = new SearchEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            if (target == DrawablePosition.RIGHT) {
                binding.etSearchInput.removeTextChangedListener(textWatcher);
                binding.etSearchInput.setText("");
                binding.etSearchInput.addTextChangedListener(textWatcher);
                binding.etSearchInput.requestFocus();
            }
        }
    };
    private MODE viewMode;
    private View.OnClickListener fabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (viewMode == MODE.LIST) {
                showMap();
            } else {
                showList();
            }
            animateFabIcon();

        }
    };
    private VenuesAdapter venuesAdapter;
    private GoogleMap googleMap;

    private void animateFabIcon() {
        ObjectAnimator.ofFloat(binding.fab, "rotation", 0f, 360f).setDuration(800).start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (viewMode == MODE.LIST) {
                    binding.fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_map_black_24dp));

                } else {
                    binding.fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_black_24dp));

                }
            }
        }, 400);
    }

    private void showSuccess(SearchResult searchResult) {
        if (!searchResult.getSearchQuery().equals(currentSearchString))
            return;
        venuesAdapter.updateList(searchResult.getSearchResponse().getResponse().getVenues());
        updateGoogleMap(searchResult.getSearchResponse().getResponse().getVenues());
    }

    private void updateGoogleMap(List<Venues> venuesList) {
        if (placeMarkerMap == null)
            placeMarkerMap = new HashMap<>();

        List<Venues> venuesToRemove = new ArrayList<>();
        for (Venues key : placeMarkerMap.keySet()) {
            if (!venuesList.contains(key)) {
                venuesToRemove.add(key);
            }
        }

        for (Venues v : venuesToRemove) {
            removeMarker(v);
        }

        for (Venues v : venuesList) {
            if (!placeMarkerMap.containsKey(v)) {
                addMarker(v);
            }
        }

        if (viewMode == MODE.MAP)
            updateMarkers();
    }

    private void addMarker(Venues v) {
        if (googleMap == null)
            return;
        LatLng latLng = new LatLng(v.getLocation().getLat(), v.getLocation().getLng());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(v.getName());
        Marker marker = googleMap.addMarker(markerOptions);
        placeMarkerMap.put(v, marker);
    }

    private void removeMarker(Venues key) {
        placeMarkerMap.get(key).remove();
        placeMarkerMap.remove(key);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);

        viewMode = MODE.LIST;

        binding.etSearchInput.addTextChangedListener(textWatcher);
        drawableLeft = binding.etSearchInput.getCompoundDrawables()[0];
        drawableRight = binding.etSearchInput.getCompoundDrawables()[2];
        binding.etSearchInput.setCompoundDrawables(drawableLeft, null, null, null);
        binding.etSearchInput.setDrawableClickListener(searchDrawableClickListener);
        binding.etSearchInput.requestFocus();
        binding.etSearchInput.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utils.showKeyboard(binding.etSearchInput);
            }
        },300);

        binding.fab.setOnClickListener(fabClickListener);

        venuesAdapter = new VenuesAdapter(this);
        binding.rv.setAdapter(venuesAdapter);
        binding.rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.rv.addItemDecoration(new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.activity_vertical_margin)));

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (savedInstanceState != null){
            currentSearchString = savedInstanceState.getString(OUT_STATE.QUERY);
        }

        viewModel = ViewModelProviders.of(this,viewModelFactory).get(PlacesViewModel.class);
        viewModel.getLiveData().observe(this, searchResultObserver);

        viewModel.fetchLikes().observe(this,likesObserver);
    }

    @Override
    public void onBackPressed() {
        if (viewMode == MODE.MAP){
            showList();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(OUT_STATE.QUERY, currentSearchString);
        outState.putParcelableArrayList(OUT_STATE.LIST, new ArrayList<Parcelable>(venuesAdapter.getList()));
    }


    SupportMapFragment mapFragment;

    private void showList() {
        binding.getRoot().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        viewMode = MODE.LIST;
        binding.rv.setVisibility(View.VISIBLE);
        binding.appBar.setVisibility(View.VISIBLE);

    }

    private void showMap() {

        Utils.hideKeyboard(binding.etSearchInput);

        binding.getRoot().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        viewMode = MODE.MAP;

        binding.rv.setVisibility(View.GONE);
        binding.appBar.setVisibility(View.GONE);

        updateMarkers();

    }

    private void updateMarkers() {
        if (googleMap == null)
            return;
        final LatLng seattle = new LatLng(BuildConfig.SEATLE_LAT, BuildConfig.SEATLE_LON);
        googleMap.addMarker(new MarkerOptions().position(seattle)
                .title("Seattle,WA"));

        if (placeMarkerMap != null && placeMarkerMap.size() > 0) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            ;
            for (Map.Entry<Venues, Marker> e : placeMarkerMap.entrySet()) {
                builder.include(e.getValue().getPosition());
            }
            builder.include(seattle);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),5));

        }else if (venuesAdapter.getItemCount() > 0){
            updateGoogleMap(venuesAdapter.getList());
        } else {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(seattle, 12f));

        }
    }

    @Override
    public void onItemClick(int position, List<Venues> venuesList) {
        Intent intent = new Intent(this,DetailsActivity.class);
        intent.putParcelableArrayListExtra(DetailsActivity.ARGS.LIST, new ArrayList<Parcelable>(venuesList));
        intent.putExtra(DetailsActivity.ARGS.POSITION, position);
        intent.putExtra(DetailsActivity.ARGS.LIKES, new ArrayList<>(venuesAdapter.getLikesSet()));
        startActivity(intent);
    }

    @Override
    public void onCheckChanged(String venues, boolean isChecked) {
        viewModel.onLikeChanged(venues,isChecked);
    }

    HashMap<Venues, Marker> placeMarkerMap;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng seattle = new LatLng(BuildConfig.SEATLE_LAT, BuildConfig.SEATLE_LON);
        googleMap.addMarker(new MarkerOptions().position(seattle)
                .title("Seatle,WA"));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(seattle, 12f));
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                for (Map.Entry<Venues, Marker> entry : placeMarkerMap.entrySet()) {
                    if (entry.getValue().equals(marker)) {
                        onItemClick(venuesAdapter.getList().indexOf(entry.getKey()),venuesAdapter.getList());
                        break;
                    }
                }
            }
        });

    }

    private enum MODE {
        LIST, MAP
    }

    interface OUT_STATE {
        String QUERY = "q";
        String LIST = "l";
    }
}
