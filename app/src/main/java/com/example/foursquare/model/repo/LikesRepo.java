package com.example.foursquare.model.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.foursquare.model.db.AppDb;
import com.example.foursquare.model.dto.VenueLikes;
import com.example.foursquare.model.dto.Venues;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class LikesRepo {


    private final AppDb appDb;

    private static ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Inject
    public LikesRepo(AppDb appDb) {
        this.appDb = appDb;
    }


    public LiveData<List<VenueLikes>> fetchLikes() {
        return appDb.venueLikesDao().getAll();
    }

    public void onLikeChanged(final String venueId, final boolean isChecked) {

        executorService.submit(new Runnable() {
            @Override
            public void run() {
                if (isChecked) {
                    appDb.venueLikesDao().insert(new VenueLikes(venueId));
                } else {
                    VenueLikes venueLikes = appDb.venueLikesDao().getByVenueId(venueId);
                    appDb.venueLikesDao().delete(venueLikes);
//                    appDb.venueLikesDao().delete(venueId);
                }
            }
        });


    }
}
