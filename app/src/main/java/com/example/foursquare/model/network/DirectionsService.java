package com.example.foursquare.model.network;


import com.example.foursquare.model.dto.DirectionsResponse;
import com.example.foursquare.model.dto.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public interface DirectionsService {

    @GET("json")
    Call<DirectionsResponse> getDirection(@Query("origin") String origin, @Query("destination") String destination );

}
