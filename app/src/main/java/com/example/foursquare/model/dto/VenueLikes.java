package com.example.foursquare.model.dto;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import javax.annotation.PropertyKey;

@Entity(indices = {@Index(value ={"venueId"},unique = true)})
public class VenueLikes {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String venueId;

    private int createDateSecs = (int) (System.currentTimeMillis()/1000);

    public VenueLikes(String venueId) {
        this.venueId = venueId;
    }

    public long getId() {
        return id;
    }

    public String getVenueId() {
        return venueId;
    }

    public int getCreateDateSecs() {
        return createDateSecs;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public void setCreateDateSecs(int createDateSecs) {
        this.createDateSecs = createDateSecs;
    }
}
