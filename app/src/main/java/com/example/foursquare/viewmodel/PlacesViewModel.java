package com.example.foursquare.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.example.foursquare.model.dto.SearchResult;
import com.example.foursquare.model.dto.VenueLikes;
import com.example.foursquare.model.dto.Venues;
import com.example.foursquare.model.repo.LikesRepo;
import com.example.foursquare.model.repo.PlacesRepo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PlacesViewModel extends BaseViewModel {

    private PlacesRepo placesRepo;

    private LikesRepo likesRepo;

    @Inject
    public PlacesViewModel(@NonNull Application application, PlacesRepo placesRepo, LikesRepo likesRepo) {
        super(application);
        this.placesRepo = placesRepo;
        this.likesRepo = likesRepo;
    }

    public void fetchPlaces(String currentSearchString) {
        placesRepo.search(currentSearchString);
    }

    public MutableLiveData<SearchResult> getLiveData() {
        return placesRepo.getLiveData();
    }

    public void onLikeChanged(String venueId, boolean isChecked) {
        likesRepo.onLikeChanged( venueId,  isChecked);
    }

    public LiveData<List<String>> fetchLikes() {
        return Transformations.map(likesRepo.fetchLikes(), new Function<List<VenueLikes>, List<String>>() {
            @Override
            public List<String> apply(List<VenueLikes> input) {
                if (input == null) return null;
                List<String> likeIds = new ArrayList<>();
                for (VenueLikes vl : input){
                    likeIds.add(vl.getVenueId());
                }
                return likeIds;
            }
        });
    }


}
