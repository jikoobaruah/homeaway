package com.example.foursquare.model.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.foursquare.model.dto.VenueLikes;

import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface VenueLikesDao {

    @Query("SELECT * FROM venuelikes")
    LiveData<List<VenueLikes>> getAll();


//    @Query("DELETE FROM venuelikes WHERE venueId = :venueId")
//    void delete(String venueId);

    @Delete
    void delete(VenueLikes venueLikes);

    @Insert
    void insert(VenueLikes venueLikes);

    @Query("SELECT * FROM venuelikes WHERE venueId = :venueId")
    VenueLikes getByVenueId(String venueId);
}
