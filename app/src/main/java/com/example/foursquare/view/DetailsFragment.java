package com.example.foursquare.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.foursquare.R;
import com.example.foursquare.databinding.FragmentDetailsBinding;
import com.example.foursquare.di.ViewModelFactory;
import com.example.foursquare.model.dto.VenueResponse;
import com.example.foursquare.model.dto.Venues;
import com.example.foursquare.view.common.BaseFragment;
import com.example.foursquare.viewmodel.DetailsViewModel;

import javax.inject.Inject;

public class DetailsFragment extends BaseFragment {

    @Inject
    ViewModelFactory viewModelFactory;

    private FragmentDetailsBinding binding;
    private DetailsViewModel viewModel;
    private boolean isLiked;

    private Venues venues;
    private String website;

    interface ARGS {
        String ITEM = "item";
        String IS_LIKED = "isLiked";
    }

    public static Fragment newInstance(Context context, Venues venues, boolean isLiked) {
        Bundle args = new Bundle();
        args.putParcelable(ARGS.ITEM, venues);
        args.putBoolean(ARGS.IS_LIKED, isLiked);
        return Fragment.instantiate(context, DetailsFragment.class.getName(), args);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        venues = getArguments().getParcelable(ARGS.ITEM);
        isLiked = getArguments().getBoolean(ARGS.IS_LIKED, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel.class);
        viewModel.getImageLiveData(venues).observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Glide.with(DetailsFragment.this).load(s).apply(new RequestOptions().fitCenter()).into(binding.expandedImage);
            }
        });

        viewModel.getVenueDetails().observe(this, new Observer<VenueResponse>() {
            @Override
            public void onChanged(@Nullable VenueResponse venueResponse) {
                if (venueResponse == null)
                    return;
                website = venueResponse.getResponse().getVenue().getCanonicalUrl();
                binding.cvWebsite.setVisibility(View.VISIBLE);
                binding.tvWebsite.setText(website);
            }
        });
        viewModel.fetchDetails(venues.getId());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setVenues(venues);
        binding.executePendingBindings();
        Glide.with(this).load(venues.getCategories().get(0).getImageUrl()).into(binding.ivCategoryIcon);
        viewModel.fetchPolyLine(venues.getId(), venues.getLocation().getLat(), venues.getLocation().getLng());
        binding.toolbar.setTitle(venues.getName());

        binding.fab.setImageResource(isLiked ? R.drawable.ic_thumb_up_pink_24dp : R.drawable.ic_thumb_up_black_24dp);
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLiked = !isLiked;
                binding.fab.setImageResource(isLiked ? R.drawable.ic_thumb_up_pink_24dp : R.drawable.ic_thumb_up_black_24dp);
                viewModel.onLikeChanged(venues.getId(), isLiked);
            }
        });

        binding.cvWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(website));
                startActivity(i);
            }
        });

    }
}
