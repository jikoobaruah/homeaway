package com.example.foursquare.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DirectionsResponse {

    private List<Route> routes;

    public List<Route> getRoutes() {
        return routes;
    }

    private String error_message;

    public String getError_message() {
        return error_message;
    }


    public static class Polyline {
        private String points;

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }

    public static class Route {

        @SerializedName("overview_polyline")
        private Polyline overviewPolyline;

        public Polyline getOverviewPolyline() {
            return overviewPolyline;
        }

        public void setOverviewPolyline(Polyline overviewPolyline) {
            this.overviewPolyline = overviewPolyline;
        }


    }
}
