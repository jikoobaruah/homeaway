package com.example.foursquare.model.repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.foursquare.model.dto.SearchResponse;
import com.example.foursquare.model.dto.SearchResult;
import com.example.foursquare.model.dto.VenueResponse;
import com.example.foursquare.model.network.PlacesService;
import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlacesRepo {

    private static final String TAG = "PlacesRepo";

    private String currentSearch;
    private Call<SearchResponse> currentCall;

    private MutableLiveData<SearchResult> searchResultLiveData;

    private MutableLiveData<VenueResponse> venueDetailsLiveData;

    private PlacesService placesService;

    @Inject
    public PlacesRepo(PlacesService placesService1){
        placesService = placesService1;
        searchResultLiveData = new MutableLiveData<>();
        venueDetailsLiveData = new MutableLiveData<>();
    }

    public void search(String currentSearchString) {
        Log.d(TAG, "search > " + currentSearchString);

        if (currentSearchString.equals(currentSearch)) {
            Log.d(TAG, "search already running> " + currentSearchString);
            return;
        }
        if (currentCall != null)
            currentCall.cancel();

        currentSearch = currentSearchString;
        currentCall = placesService.search(currentSearch);
        Log.d(TAG, "enqueue request> " + currentSearch);
        post(SearchResult.getFetching(currentSearch));
        currentCall.enqueue(new SearchResponseCallback(currentSearch));

    }



    public MutableLiveData<SearchResult> getLiveData() {
        return searchResultLiveData;
    }

    public LiveData<VenueResponse> getDetailsLiveData() {
        return venueDetailsLiveData;
    }

    public void fetchDetails(String venueId){
        placesService.details(venueId).enqueue(new Callback<VenueResponse>() {
            @Override
            public void onResponse(Call<VenueResponse> call, Response<VenueResponse> response) {
                if (response.isSuccessful() && response.body() != null)
                    venueDetailsLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<VenueResponse> call, Throwable t) {

            }
        });
    }


    class SearchResponseCallback implements Callback<SearchResponse>{


        private final String currentQuery;

        public SearchResponseCallback(String currentQuery) {
            this.currentQuery = currentQuery;
        }

        @Override
        public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
            if (response.isSuccessful()){
                Log.d(TAG,"success request> "+ call.request().url() );
                Log.d(TAG,"success response> "+ response.body().toString() );
                if (response.body().getResponse().getVenues() == null || response.body().getResponse().getVenues().size() == 0){
                    post(SearchResult.getFetchingFail(currentQuery,"NO items found"));
                }else{
                    post(SearchResult.getFetchingSuccess(currentQuery,response.body()));
                }

            }else{
                handleError(call, response.errorBody());
            }
        }

        @Override
        public void onFailure(Call<SearchResponse> call, Throwable t) {
            if (call.isCanceled()) {
                Log.d(TAG, "cancelled >> " + call.request().url());
                post(SearchResult.getFetchingCancelled(currentQuery));
            } else {
                Log.d(TAG, "failed >> " + call.request().url() + " :: "+t.getMessage());
                post(SearchResult.getFetchingFail(currentQuery,t.getMessage()));
            }

        }

        private void handleError(Call<SearchResponse> call, ResponseBody errorBody) {
            try {
                SearchResponse response = new Gson().fromJson(errorBody.string(),SearchResponse.class);
                post(SearchResult.getFetchingFail(currentQuery,response.getMeta().getErrorDetail()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void post(SearchResult result) {
        searchResultLiveData.postValue(result);

    }


}
