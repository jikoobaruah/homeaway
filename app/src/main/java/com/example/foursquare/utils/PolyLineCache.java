package com.example.foursquare.utils;

import android.content.Context;

import com.example.foursquare.App;

import java.util.HashMap;

public class PolyLineCache {

    private HashMap<String,String> polyMap;

    private Context appContext;

    private PolyLineCache(){
        polyMap = new HashMap<>();
        appContext = App.getAppContext();
    }

    private static PolyLineCache sInstance;

    public static PolyLineCache getInstance() {
        if (sInstance == null)
            sInstance =new PolyLineCache();
        return sInstance;
    }

    public String get(String venueId){
        return polyMap.get(venueId);
    }

    public void put(String venueId, String polyLine){
        polyMap.put(venueId,polyLine);
    }
}
