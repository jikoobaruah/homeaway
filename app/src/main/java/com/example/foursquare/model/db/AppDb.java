package com.example.foursquare.model.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.foursquare.model.db.dao.VenueLikesDao;
import com.example.foursquare.model.dto.VenueLikes;

@Database(entities = {VenueLikes.class}, version = 1)
public abstract class AppDb extends RoomDatabase {
    public abstract VenueLikesDao venueLikesDao();
}