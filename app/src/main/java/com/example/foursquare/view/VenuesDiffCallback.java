package com.example.foursquare.view;

import android.support.v7.util.DiffUtil;

import com.example.foursquare.model.dto.Venues;

import java.util.List;

public class VenuesDiffCallback extends DiffUtil.Callback {

    private final List<Venues> oldList;
    private final List<Venues> newList;

    public VenuesDiffCallback(List<Venues> oldVenusList, List<Venues> newVenuesList) {
        this.oldList = oldVenusList;
        this.newList = newVenuesList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId().equals(newList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Venues oldVenus = oldList.get(oldItemPosition);
        final Venues newVenues = newList.get(newItemPosition);

        return oldVenus.getName().equals(newVenues.getName());
    }

    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}