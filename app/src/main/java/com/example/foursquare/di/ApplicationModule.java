package com.example.foursquare.di;

import android.arch.persistence.room.Room;

import com.example.foursquare.App;
import com.example.foursquare.BuildConfig;
import com.example.foursquare.R;
import com.example.foursquare.model.db.AppDb;
import com.example.foursquare.model.network.DirectionsService;
import com.example.foursquare.model.network.PlacesService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
@Module(includes = ViewModelModule.class)
public class ApplicationModule {

    private static final String BASE_URL = "https://api.foursquare.com/v2/venues/";
    private static final String DIRECTIONS_BASE_URL = "https://maps.googleapis.com/maps/api/directions/";

    @Singleton
    @Provides
    static Retrofit provideRetrofit() {
        File httpCacheDirectory = new File(App.getAppContext().getCacheDir(), "responses");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        OkHttpClient.Builder builder = new OkHttpClient.Builder().cache(cache);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                HttpUrl url = request.url().newBuilder().
                        addQueryParameter("client_secret", BuildConfig.CLIENT_SECRET).
                        addQueryParameter("client_id", BuildConfig.CLIENT_ID).
                        addQueryParameter("near", "Seattle,+WA").
                        addQueryParameter("v", new SimpleDateFormat("YYYYMMDD").format(new Date())).
//                            addQueryParameter("ll","Seattle,+WA").
        build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

       return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    @Singleton
    @Provides
    static PlacesService provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(PlacesService.class);
    }


    @Singleton
    @Provides
    @Named("directions")
    static Retrofit provideDirectionsRetrofit() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                HttpUrl url = request.url().newBuilder().
                        addQueryParameter("key", App.getAppContext().getString(R.string.google_maps_key)).
        build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        return new Retrofit.Builder()
                .baseUrl(DIRECTIONS_BASE_URL)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    @Singleton
    @Provides
    static DirectionsService provideDirectionService(@Named("directions") Retrofit retrofit) {
        return retrofit.create(DirectionsService.class);
    }

    @Singleton
    @Provides
    static AppDb provideDb() {
        return Room.databaseBuilder(App.getAppContext(), AppDb.class, "app.db").build();
    }
}
