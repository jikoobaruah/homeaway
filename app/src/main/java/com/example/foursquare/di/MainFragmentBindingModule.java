package com.example.foursquare.di;


import com.example.foursquare.view.DetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBindingModule {


    @ContributesAndroidInjector
    abstract DetailsFragment provideDetailsFragment();
}