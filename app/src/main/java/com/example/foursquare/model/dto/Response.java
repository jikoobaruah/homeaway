package com.example.foursquare.model.dto;

import com.google.gson.Gson;

import java.util.List;

public class Response {

    private List<Venues> venues;

    public List<Venues> getVenues ()
    {
        return venues;
    }

    public void setVenues (List<Venues> venues)
    {
        this.venues = venues;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
