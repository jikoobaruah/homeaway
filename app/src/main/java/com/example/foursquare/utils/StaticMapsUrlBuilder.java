package com.example.foursquare.utils;

import com.example.foursquare.App;
import com.example.foursquare.R;

import java.util.ArrayList;
import java.util.List;

public class StaticMapsUrlBuilder {

    private String baseUrl = "https://maps.googleapis.com/maps/api/staticmap?";

    private List<String> params;

    public StaticMapsUrlBuilder(){
        params = new ArrayList<>();
    }

    public StaticMapsUrlBuilder addMarker(String color, String label, double lat, double lon){
        params.add(String.format("markers=size:medium|color:%s|label:%s|%s,%s",color,label,lat,lon));
        return this;
    }

    public StaticMapsUrlBuilder size(int width, int height){
        params.add(String.format("size=%sx%s",width,height));
        return this;
    }

    public StaticMapsUrlBuilder path(int weight, String color, String polyLine){
        params.add(String.format("path=weight:%s|color:%s|enc:%s",weight,color,polyLine));
        return this;
    }

    public String build(){
        StringBuilder stringBuilder = new StringBuilder(baseUrl);
        for (String s : params){
            stringBuilder.append(s).append("&");
        }
        stringBuilder.append("key="+ App.getAppContext().getString(R.string.google_maps_key));

        return stringBuilder.toString();
    }


}
