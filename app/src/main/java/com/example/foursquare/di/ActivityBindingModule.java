package com.example.foursquare.di;

import com.example.foursquare.view.DetailsActivity;
import com.example.foursquare.view.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract DetailsActivity bindDetailsActivity();
}
