package com.example.foursquare.model.repo;

import android.arch.lifecycle.MutableLiveData;

import com.example.foursquare.BuildConfig;
import com.example.foursquare.model.dto.DirectionsResponse;
import com.example.foursquare.model.dto.SearchResponse;
import com.example.foursquare.model.dto.SearchResult;
import com.example.foursquare.model.network.DirectionsService;
import com.example.foursquare.model.network.PlacesService;
import com.example.foursquare.utils.PolyLineCache;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectionsRepo {
    private static final String TAG = "DirectionsRepo";


    private MutableLiveData<String> searchResultLiveData;


    private DirectionsService directionsService;

    @Inject
    public DirectionsRepo(DirectionsService service){
        directionsService = service;
        searchResultLiveData = new MutableLiveData<>();
    }

    public void getDirections(final String venueId, double lat, double lon) {

        if (PolyLineCache.getInstance().get(venueId) != null){
            searchResultLiveData.setValue(PolyLineCache.getInstance().get(venueId));
            return;
        }

        String origin = BuildConfig.SEATLE_LAT+","+BuildConfig.SEATLE_LON;
        String destination = lat+","+lon;
        directionsService.getDirection(origin,destination).enqueue(new DirectionCallback(venueId));

    }

    public MutableLiveData<String> getLiveData() {
        return searchResultLiveData;
    }

    private class DirectionCallback implements Callback<DirectionsResponse> {
        private final String venueId;

        public DirectionCallback(String venueId) {
            this.venueId = venueId;
        }

        @Override
        public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
            if (response.isSuccessful() && response.body().getRoutes() != null && response.body().getRoutes().size() > 0) {
                PolyLineCache.getInstance().put(venueId,response.body().getRoutes().get(0).getOverviewPolyline().getPoints());
                searchResultLiveData.postValue(response.body().getRoutes().get(0).getOverviewPolyline().getPoints());
            } else
                searchResultLiveData.postValue(null);
        }

        @Override
        public void onFailure(Call<DirectionsResponse> call, Throwable t) {
            searchResultLiveData.postValue(null);

        }
    }
}
